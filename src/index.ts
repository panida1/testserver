import express, { RequestHandler } from "express";

const app = express();
const port = 3333;

const path = "/home/:roomid/:floor";

app.get(path, (req, res, next) => {
  const { room, floor } = req.params;
  res.json({ room, floor });
});

app.listen(port, () => console.log(`Hello world from ${port}!`));
