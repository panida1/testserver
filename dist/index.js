"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = express_1.default();
const port = 3333;
const path = "/home/:roomid/:floor";
app.get(path, (req, res, next) => {
    const { room, floor } = req.params;
    res.send({ room, floor });
});
app.listen(port, () => console.log(`Hello world from ${port}!`));
